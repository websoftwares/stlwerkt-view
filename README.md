# Gezondtransport\View

Class voor het renderen van een view

### Composer
Gebruik composer om de dependencies te installeren

```
php composer.phar install
```

### Handleiding

Plaats in een map php view bestanden bijvoorbeeld:

Default map locatie en naam

```php
vendor/package/src/Namespace/config/views
```

Nu kan je naar het bestand view de view class variablen sturen en daarna de output tonen.

view.html.php:

```
<p><?= $test;?></p>
```

Gebruik bestandsnaam zonder extensie.

```
use Gezondtransport\View;

$file =  'view.html';

// Setup the view
$view = new View($file, $variables = array(
        'test' => 'Hello World!'
    ));

echo $view ->render();
```

<?php
/**
 * View
 * Class voor het renderen van een view
 *
 * @package Gezondtransport
 * @author Boris <boris@gezondtransport.nl
 */
namespace Gezondtransport;

class View
{
    /**
     * $file
     * @var string
     */
    public $file = null;

    /**
     * $variables
     * @var array
     */

    public $variables = null;
    /**
     * $path
     * @var string
     */
    public $path = null;

    public function __construct($file = null, array $variables = array(), $path = null)
    {
        if (! $file) {
            throw new \InvalidArgumentException($file . 'file cannot be empty');
        }
        if (! $variables) {
            throw new \InvalidArgumentException($variables . 'variables cannot be empty');
        }

        $this->file = $file;
        $this->variables = $variables;
        $this->path = $path;
    }

    /**
     * Getter for variables
     *
     * @return mixed
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * Setter for variables
     *
     * @param  mixed $variables Value to set
     * @return self
     */
    public function setVariables($key, $value)
    {
        $this->variables[$key] = $value;

        return $this;
    }

    /**
     * Getter for path
     *
     * @return mixed
     */
    public function getPath()
    {
        if (! $this->path) {
            $this->path =  dirname(__FILE__) . '/views/';
        }

        if (! file_exists($this->path . $this->file . '.php')) {
            throw new \OutOfRangeException("the file " . $this->path . $this->file . '.php' ." could not be retrieved");
        }

        return $this->path;
    }

    /**
     * Setter for path
     *
     * @param  mixed $path Value to set
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * render a view file
     *
     * @return void
     */
    public function render()
    {
        extract($this->variables);

        ob_start();
        include $this->getPath(). $this->file .'.php';
        $renderedView = ob_get_clean();

        return $renderedView;
    }
}

<?php

use Gezondtransport\View;

class ViewTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->file =  'test.html';

        // Setup the view
        $this->view = new View($this->file, $variables = array(
                'test' => 'Unit_Test'
            ));
    }

    public function testInstantiateAsObjectSucceeds()
    {
        $this->assertInstanceOf('Gezondtransport\View', $this->view);
    }

    public function testRenderSucceeds()
    {
        $expected = '<p>Unit_Test</p>';
        $this->assertInternalType('string',$this->view->render());
        $this->assertEquals($expected,trim($this->view->render()));
    }

    public function testSetVariableSucceeds()
    {
        $this->view->setVariables('test2', 'UnitTesting2');
        $expected = array('test' => 'Unit_Test', 'test2' => 'UnitTesting2');
        $actual = $this->view->getVariables();
        $this->assertEquals($expected, $actual);

    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInstantiateAsObjectFileFails()
    {
        new View();
    }

    /**
     * @expectedException OutOfRangeException
     */
    public function testRenderFileFails()
    {
        $file = 'notFound';

        $view = new View($file, $variables = array(
                'dummy'
            ));

        $view->render();
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function  testInstantiateAsObjectVariablesFails()
    {
        new View($this->file);
    }
}
